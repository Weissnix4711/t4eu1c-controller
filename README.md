# Sonoff T4EU1C custom control board w/ WS2812B LEDs and DALLAS sensor

## PCB

![PCB Front](docs/render_pcb_front.png)
![PCB Back](docs/render_pcb_back.png)

## Assembly

![PCBA Front](docs/render_pcba_front.png)
![PCBA Back](docs/render_pcba_back.png)

## License

TBD
